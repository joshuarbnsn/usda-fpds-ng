package gov.usda.nfc.dataact;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ParseProcurementData {
	private static final int BUFFER_SIZE = 4096;
	
	public String parseFiscalYearLinks(String html)
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		Document doc = Jsoup.parse(html);
		Elements links = doc.select("a[href*=FFY]"); // a with href
		for (Element link : links)
		{
			String ref = link.attr("href");
			int indexOfFFY = ref.indexOf("FFY") + 3;
			String year = ref.substring(indexOfFFY, indexOfFFY + 2);
			int indexofVersion = ref.indexOf("-V") + 2;
			int indexofVersionEnd = ref.indexOf("&");
			String version = ref.substring(indexofVersion, indexofVersionEnd);
			String key = year + version;
			//Double d = Double.parseDouble(key);
			map.put(key, ref);
		}
		
		String result = map.lastEntry().getValue();
		
		return result;
	}
	public String parseDeparmentLinks(String html)
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		Document doc = Jsoup.parse(html);
		Elements links = doc.select("a[href*=AGRICULTUREDEPARTMENTOF]"); // a with href

		String result = links.first().attr("href");
		
		return result;
	}
	public String parseZipLinks(String html)
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		Document doc = Jsoup.parse(html);
		Elements links = doc.select("a[href*=AGRICULTUREDEPARTMENTOF]"); // a with href

		String result = links.first().attr("href");
		result = result.substring(3);
		return result;
	}
	
	public  List<String> unzipFiles(String fileName, String directoryExtension, String basePath) throws IOException 
	{
		List<String> unzippedFilePaths = new ArrayList<String>();
		String zipFilePath = basePath + File.separator + fileName;
		String destDirectory = zipFilePath + directoryExtension;
		File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        
        File zipFile = new File(zipFilePath);
        int i = 0;
        
        while (!zipFile.exists() && i < 5) 
        {
        	try {
        		Thread.sleep(10000);
        	} catch (Exception e) {}
        	
        	//increment the counter
        	i++;
        	
        	zipFile = new File(zipFilePath);
        }
        
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFile));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            Path path = Paths.get(filePath);
            filePath = path.toString();
            
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
                //add filePath to list
                unzippedFilePaths.add(filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        
        zipIn.close();
        return unzippedFilePaths;
	}
	
	public List<String> readOutputDir(String filePath) {
		List<String> unzippedFilePaths = new ArrayList<String>();
		File dir = new File(filePath);
		File[] fileArray = dir.listFiles();
		
		int i;
		for (i=0; i<fileArray.length; i++) {
			unzippedFilePaths.add(fileArray[i].getAbsolutePath());
		}
		
		return unzippedFilePaths;
	}
	private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
	
	public BufferedInputStream readFile(String filePath) throws IOException {
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(filePath));
		return is;
	}

}
